var NAVTREE =
[
  [ "orx", "index.html", [
    [ "Todo List", "todo.html", null ],
    [ "Modules", "modules.html", "modules" ],
    [ "Data Structures", "annotated.html", [
      [ "Data Structures", "annotated.html", "annotated_dup" ],
      [ "Data Structure Index", "classes.html", null ],
      [ "Data Fields", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Variables", "functions_vars.html", "functions_vars" ]
      ] ]
    ] ],
    [ "Files", null, [
      [ "File List", "files.html", "files" ],
      [ "Globals", "globals.html", [
        [ "All", "globals.html", "globals_dup" ],
        [ "Functions", "globals_func.html", "globals_func" ],
        [ "Variables", "globals_vars.html", null ],
        [ "Typedefs", "globals_type.html", null ],
        [ "Enumerations", "globals_enum.html", null ],
        [ "Enumerator", "globals_eval.html", "globals_eval" ],
        [ "Macros", "globals_defs.html", "globals_defs" ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"annotated.html",
"group__orx_anim_pointer.html#ga7c5c2dcb63581a6cedcce6d287beea33",
"group__orx_body.html#gae2d0e00dc1fb881d37c2dcd1ab07d3cb",
"group__orx_command.html#gga579c8c869f7eaf4fda8655892ea734aaa6110c62e084dcf5044efd852c70e3495",
"group__orx_debug.html#ga443bd0cd35cd0d35d9f34d117ab7f84d",
"group__orx_display.html#ga0bbde87afcc4090fd37efd155b5568df",
"group__orx_event.html#gga4576598772ee9b827a80ca5b6c9a6cd8a3f6047dad21b480b5a982b8223e6a2e9",
"group__orx_font.html#ga1ba89979fcb1047295effd392c4fa8e9",
"group__orx_hash_table.html",
"group__orx_joystick.html#ggab6cecefc4e8f1578ed021880bb9cc94ba1e24a2a9e41e036c913e816014603ac5",
"group__orx_keyboard.html#gga50a25da712297bb6cbbd47e66ac233fea77bcf4646717176fa1870c60cf92129d",
"group__orx_math.html#gaf584502f6429224677bcf8d5ddb29104",
"group__orx_o_box.html#ga667a5d563aeab011b05e09d2f02d06f0",
"group__orx_object.html#gaeac04e584788022c4bfb0e0e72ac0533",
"group__orx_physics.html#gga1c4aaa4ece57611bd21ae6470a07a29dab29cafaf8e8cecac3431a4a88a5d5849",
"group__orx_plugin.html#ggab155bd6d5a09b625a08a9fc69c35bcb7a03896fbd0f69d41f8c93d64297dbd41a",
"group__orx_profiler.html#ga7a3ec1d7238daade394651ed47909a11",
"group__orx_shader.html#gafb33ac8cb6d593ce19a1c84a5b8f59b3",
"group__orx_sound_system.html#ga264653cca01906bd3d7004a4fe0965e7",
"group__orx_string.html#ga6f2d9f112ec01738725cd001d7d17ae6",
"group__orx_structure.html#gga559293f09cd4d21743241cc32350b320a8e26d5803fc825cfe4c3195098af6d16",
"group__orx_tree.html#ga26ed9987077dc6e4918205ac3dac360a",
"group__orx_viewport.html#ga583441b79aeaa36dd5fd166ca7c98138",
"structorx_a_n_d_r_o_i_d___j_o_y_s_t_i_c_k___e_v_e_n_t.html#a6f1cfa7e832d15bb3ba471e63f75d712",
"structorx_r_e_s_o_u_r_c_e___t_y_p_e___i_n_f_o.html#a7cfd554c9170f173cea714f304b254a3"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';